
// task 1

const product = {
  name: "Car",
  price: 20000,
  discount: 15,

  showfunc: function () {
    let newPrice = this.price * (1 - this.discount / 100);
    return newPrice;
  },
};

let result = product.showfunc();
console.log(result)

// task 2


function showUser(user) {
  return `Привіт я ${user.name}, мені ${user.age} років.`;
}

let userName = prompt("Введіть своє імя:");
let userAge = prompt("Введіть свій вік:");

const user = {
  name: userName,
  age: userAge,
};

console.log(user);

const showResult = showUser(user);
alert(showResult);


